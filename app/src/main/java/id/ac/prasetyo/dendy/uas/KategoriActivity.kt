package id.ac.prasetyo.dendy.uas

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import kotlinx.android.synthetic.main.activity_kategori.*

class KategoriActivity : AppCompatActivity(),  View.OnClickListener {

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnNovel -> {
                val intent = Intent(this, BukuKategori::class.java)
                intent.putExtra("nama_kategori","novel")
                startActivity(intent)
            }
            R.id.btnUmum -> {
                val intent = Intent(this, BukuKategori::class.java)
                intent.putExtra("nama_kategori","umum")
                startActivity(intent)
            }
            R.id.btnSosial -> {
                val intent = Intent(this, BukuKategori::class.java)
                intent.putExtra("nama_kategori","sosial")
                startActivity(intent)
            }
            R.id.btnTekno -> {
                val intent = Intent(this, BukuKategori::class.java)
                intent.putExtra("nama_kategori","teknologi")
                startActivity(intent)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kategori)

        btnUmum.setOnClickListener(this)
        btnNovel.setOnClickListener(this)
        btnSosial.setOnClickListener(this)
        btnTekno.setOnClickListener(this)

    }
}