package id.ac.prasetyo.dendy.uas

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.tampil_kategori.*
import org.json.JSONArray

class BukuKategori : AppCompatActivity() {

    lateinit var kategoriadapter : AdapterBuku
    val daftarkategori = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.0.112/uas/kategori.php"
    var a : String? = ""
    var b : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tampil_kategori)

        kategoriadapter = AdapterBuku(daftarkategori, BukuActivity())
        listKategori.layoutManager = LinearLayoutManager(this)
        listKategori.adapter = kategoriadapter

        var paket : Bundle? = intent.extras
        a = paket?.getString("nama_kategori")

    }

    override fun onStart() {
        super.onStart()
        showdatakategori(a.toString())
    }

    fun showdatakategori(namakategori : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarkategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var buku = HashMap<String,String>()
                    buku.put("id_buku",jsonObject.getString("id_buku"))
                    buku.put("judul_buku",jsonObject.getString("judul_buku"))
                    buku.put("pengarang",jsonObject.getString("pengarang"))
                    buku.put("tahun",jsonObject.getString("tahun"))
                    buku.put("penerbit",jsonObject.getString("penerbit"))
                    buku.put("sinopsis",jsonObject.getString("sinopsis"))
                    buku.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    buku.put("nama_rak",jsonObject.getString("nama_rak"))
                    buku.put("url",jsonObject.getString("url"))
                    daftarkategori.add(buku)
                }
                kategoriadapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama_kategori",namakategori)
                return hm
            }
        }

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}