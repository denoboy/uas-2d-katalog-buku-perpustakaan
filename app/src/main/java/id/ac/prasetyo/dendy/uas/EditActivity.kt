package id.ac.prasetyo.dendy.uas

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_edit.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class EditActivity : AppCompatActivity() {

    lateinit var adapteredit : adapterEdit
    val daftarMo = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.0.114/uas/show_data.php"
    val url2 = "http://192.168.0.114/uas/query_ins_upd_del.php"
    var id_buku = ""
    var judul = ""
    var pengarang = ""
    var tahun = ""
    var sinopis = ""
    var penerbit = ""
    var kategori = ""
    var rak = ""
    var cover = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        adapteredit = adapterEdit(daftarMo, this)
        var list = findViewById<RecyclerView>(R.id.listJudul)
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapteredit

        btnUpdate.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, updateActivity::class.java)
            intent.putExtra("id_buku",id_buku)
            intent.putExtra("judul_buku",judul)
            intent.putExtra("tahun",tahun)
            intent.putExtra("pengarang",pengarang)
            intent.putExtra("penerbit",penerbit)
            intent.putExtra("sinopsis",sinopis)
            intent.putExtra("url",cover)
            startActivity(intent)
        })


    }


override fun onStart() {
    super.onStart()
    showDataMo()
}


fun showDataMo(){
    val request = StringRequest(
        Request.Method.POST,url,
        Response.Listener { response ->
            daftarMo.clear()
            val jsonArray = JSONArray(response)
            for(x in 0 .. (jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var buku = HashMap<String,String>()
                buku.put("id_buku",jsonObject.getString("id_buku"))
                buku.put("judul_buku",jsonObject.getString("judul_buku"))
                buku.put("pengarang",jsonObject.getString("pengarang"))
                buku.put("tahun",jsonObject.getString("tahun"))
                buku.put("penerbit",jsonObject.getString("penerbit"))
                buku.put("sinopsis",jsonObject.getString("sinopsis"))
                buku.put("nama_kategori",jsonObject.getString("nama_kategori"))
                buku.put("nama_rak",jsonObject.getString("nama_rak"))

                buku.put("url",jsonObject.getString("url"))

                daftarMo.add(buku)
            }
            adapteredit.notifyDataSetChanged()
        },
        Response.ErrorListener { error ->
            Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
        })
    val queue = Volley.newRequestQueue(this)
    queue.add(request)
}

fun queryInsertUpdateDelete(mode : String){
    val request = object : StringRequest(
        Method.POST,url2,
        Response.Listener { response ->
            val jsonObject = JSONObject(response)
            val error = jsonObject.getString("kode")
            if(error.equals("000")){
                Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                showDataMo()
            }
            else{
                Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
            }
        },
        Response.ErrorListener { error ->
            Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
        }){
        override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                Date()
            )+".jpg"
            when(mode){
//                    "insert" ->{
//                        hm.put("mode","insert")
//                        hm.put("nama_movie",edNamaMovie.text.toString())
//                        hm.put("tahun_movie",edTahunMovie.text.toString())
//                        hm.put("sinopsis_movie",edSinopsisMovie.text.toString())
//                        hm.put("image",imStr)
//                        hm.put("file",nmFile)
//                        hm.put("nama_genre",pilihGenre)
//                    }
//                    "update" ->{
//                        hm.put("mode","update")
//                        hm.put("id_movie",id_movie.toString())
//                        hm.put("nama_movie",edNamaMovie.text.toString())
//                        hm.put("tahun_movie",edTahunMovie.text.toString())
//                        hm.put("sinopsis_movie",edSinopsisMovie.text.toString())
//                        hm.put("image",imStr)
//                        hm.put("file",nmFile)
//                        hm.put("nama_genre",pilihGenre)
//                    }
                "delete" ->{
                    hm.put("mode","delete")
                    hm.put("id_buku",id_buku.toString())
                }
            }
            return hm
        }
    }
    val queue = Volley.newRequestQueue(this)
    queue.add(request)
}
}