package id.ac.prasetyo.dendy.uas

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_edit.*

class adapterEdit(val dataBK : List<HashMap<String,String>>,
                  val editActivity: EditActivity):
RecyclerView.Adapter<adapterEdit.HolderdataEdit>(){
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): adapterEdit.HolderdataEdit {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_edit,p0,false)
        return HolderdataEdit (v)
    }

    override fun getItemCount(): Int {
        return dataBK.size
    }

    override fun onBindViewHolder(p0: adapterEdit.HolderdataEdit, p1: Int) {
        val data = dataBK.get(p1)
        p0.judul.setText(data.get("judul_buku"))
        p0.nomor.setText(data.get("id_buku"))

        p0.Cedit.setOnClickListener(View.OnClickListener {
            editActivity.txout.setText(data.get("judul_buku"))
            editActivity.judul = data.get("judul_buku").toString()
            editActivity.id_buku = data.get("id_buku").toString()
            editActivity.pengarang = data.get("pengarang").toString()
            editActivity.tahun = data.get("tahun").toString()
            editActivity.sinopis = data.get("sinopsis").toString()
            editActivity.penerbit = data.get("penerbit").toString()
            editActivity.cover = data.get("url").toString()
        })
    }

    inner class HolderdataEdit(v : View) : RecyclerView.ViewHolder(v){
        val Cedit = v.findViewById<ConstraintLayout>(R.id.Cedit)
        val judul = v.findViewById<TextView>(R.id.judul)
        val nomor = v.findViewById<TextView>(R.id.nomor)
    }


}