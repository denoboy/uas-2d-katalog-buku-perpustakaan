package id.ac.prasetyo.dendy.uas

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.insert_buku.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class updateActivity : AppCompatActivity() {

    lateinit var mediaHelper: MediaHelper


    lateinit var kategoriAdapter : ArrayAdapter<String>
    var kategori= mutableListOf<String>()

    lateinit var rakAdapter : ArrayAdapter<String>
    var rak= mutableListOf<String>()

    val url = "http://192.168.0.114/uas/get_nama_kategori.php"
    val url2 = "http://192.168.0.114/uas/query_ins_upd_del.php"
    val url3 = "http://192.168.0.114/uas/get_nama_rak.php"
    var imStr = ""
    var pilihGenre = ""
    var pilihRak = ""
    var id_buku : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.insert_buku)

        mediaHelper = MediaHelper(this)

        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,kategori)
        spK.adapter = kategoriAdapter
        spK.onItemSelectedListener = itemSelected

        rakAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,rak)
        spR.adapter = rakAdapter
        spR.onItemSelectedListener = itemSelected2

        var paket : Bundle? = intent.extras
        id_buku = paket?.getString("id_buku")
        inJudul.setText(paket?.getString("judul_buku"))
        inPengarang.setText(paket?.getString("pengarang"))
        inTahun.setText(paket?.getString("tahun"))
        inPenerbit.setText(paket?.getString("penerbit"))
        inSinopsis.setText(paket?.getString("sinopsis"))

        Picasso.get().load(paket?.getString("url")).into(imageView)

        btnSimpan.setOnClickListener(View.OnClickListener {
            queryInsertUpdateDelete("update")
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data, imageView)
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(p0: AdapterView<*>?) {
            spK.setSelection(0)
            pilihGenre = kategori.get(0)
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            pilihGenre = kategori.get(p2)
        }
    }

    val itemSelected2 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(p0: AdapterView<*>?) {
            spK.setSelection(0)
            pilihRak = rak.get(0)
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            pilihRak = rak.get(p2)
        }
    }

    override fun onStart() {
        super.onStart()
        getKategori()
        getRak()
    }

    fun getKategori(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                kategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    kategori.add(jsonObject.getString("nama_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getRak(){
        val request = StringRequest(
            Request.Method.POST,url3,
            Response.Listener { response ->
                kategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    rak.add(jsonObject.getString("nama_rak"))
                }
                rakAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()

                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                    Date()
                )+".jpg"
                when(mode){
//                    "insert" ->{
//                        hm.put("mode","insert")
//                        hm.put("nama_movie",edNamaMovie.text.toString())
//                        hm.put("tahun_movie",edTahunMovie.text.toString())
//                        hm.put("sinopsis_movie",edSinopsisMovie.text.toString())
//                        hm.put("image",imStr)
//                        hm.put("file",nmFile)
//                        hm.put("nama_genre",pilihGenre)
//                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_buku",id_buku.toString())
                        hm.put("judul_buku",inJudul.text.toString())
                        hm.put("penerbit",inPenerbit.text.toString())
                        hm.put("pengarang",inPengarang.text.toString())
                        hm.put("tahun",inTahun.text.toString())
                        hm.put("sinopsis",inSinopsis.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_kategori",pilihGenre)
                        hm.put("nama_rak",pilihRak)
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_buku",id_buku.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}