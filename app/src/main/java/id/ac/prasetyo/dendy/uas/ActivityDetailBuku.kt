package id.ac.prasetyo.dendy.uas

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_buku.*

class ActivityDetailBuku : AppCompatActivity() {

    val url = "http://192.168.0.112/uas/show_data.php"

    var id_buku : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buku)

        var paket : Bundle? = intent.extras
        id_buku = paket?.getString("id_buku")
        txJdl.setText(paket?.getString("judul_buku"))
        txPengarang.setText(paket?.getString("pengarang"))
        txThn.setText(paket?.getString("tahun"))
        txKategori.setText(paket?.getString("nama_kategori"))
        txSinopsis.setText(paket?.getString("sinopsis"))
        txRak.setText(paket?.getString("nama_rak"))

        Picasso.get().load(paket?.getString("url")).into(imageV)
    }
}