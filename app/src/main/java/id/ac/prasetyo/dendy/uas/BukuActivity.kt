package id.ac.prasetyo.dendy.uas

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_buku.*
import kotlinx.android.synthetic.main.tampil_buku.*
import org.json.JSONArray

class BukuActivity: AppCompatActivity(){
    lateinit var bukuadapter : AdapterBuku
    var daftarbuku = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.0.112/uas/show_data.php"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tampil_buku)

        bukuadapter = AdapterBuku(daftarbuku, this)

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = bukuadapter

    }



    override fun onStart() {
        super.onStart()
        showDataBuku()

    }

    fun showDataBuku(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarbuku.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var buku = HashMap<String,String>()
                    buku.put("id_buku",jsonObject.getString("id_buku"))
                    buku.put("judul_buku",jsonObject.getString("judul_buku"))
                    buku.put("pengarang",jsonObject.getString("pengarang"))
                    buku.put("tahun",jsonObject.getString("tahun"))
                    buku.put("penerbit",jsonObject.getString("penerbit"))
                    buku.put("sinopsis",jsonObject.getString("sinopsis"))
                    buku.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    buku.put("nama_rak",jsonObject.getString("nama_rak"))

                    buku.put("url",jsonObject.getString("url"))
                    daftarbuku.add(buku)
                }
                bukuadapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}