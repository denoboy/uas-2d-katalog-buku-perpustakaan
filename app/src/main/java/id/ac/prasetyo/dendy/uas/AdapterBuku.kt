package id.ac.prasetyo.dendy.uas

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterBuku (val databuku : List<HashMap<String,String>>,
                   val adapterdetail:BukuActivity) :
    RecyclerView.Adapter<AdapterBuku.HolderDataBuku>(){

    inner class HolderDataBuku(v : View) : RecyclerView.ViewHolder(v){
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val image = v.findViewById<ImageView>(R.id.imageCover)
        val cons = v.findViewById<ConstraintLayout>(R.id.c1)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderDataBuku {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_buku,parent,false)
        return HolderDataBuku(v)
    }

    override fun getItemCount(): Int {
        return databuku.size
    }

    override fun onBindViewHolder(holder: HolderDataBuku, position: Int) {
        val data = databuku.get(position)
        holder.txJudul.setText(data.get("judul_buku"))


        if(position.rem(2) == 0) holder.cons.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cons.setBackgroundColor(Color.rgb(255,255,245))

        holder.cons.setOnClickListener ( View.OnClickListener {

            val intent = Intent(adapterdetail, ActivityDetailBuku::class.java)
            intent.putExtra("id_buku",data.get("id_buku"))
            intent.putExtra("judul_buku",data.get("judul_buku"))
            intent.putExtra("pengarang",data.get("pengarang"))
            intent.putExtra("tahun",data.get("tahun"))
            intent.putExtra("nama_kategori",data.get("nama_kategori"))
            intent.putExtra("sinopsis",data.get("sinopsis"))
            intent.putExtra("nama_rak",data.get("nama_rak"))
            intent.putExtra("url",data.get("url"))


            adapterdetail.startActivity(intent)
        } )
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.image);

    }
}