package id.ac.prasetyo.dendy.uas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener  {

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnKlg -> {
                val intent = Intent(this, BukuActivity::class.java)
                startActivity(intent)
            }
            R.id.btnKategori -> {
                val intent = Intent(this, KategoriActivity::class.java)
                startActivity(intent)
            }
            R.id.btnInsert -> {
                val intent = Intent(this, InsertActivity::class.java)
                startActivity(intent)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnKlg.setOnClickListener(this)
        btnKategori.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.editdata,menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
       when(item?.itemId){
           R.id.eddata ->{
               val intent = Intent(this, EditActivity::class.java)
               startActivity(intent)
               return true
           }
       }
        return super.onOptionsItemSelected(item)
    }
}